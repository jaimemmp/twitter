class AddColumnReplyToToMicroposts < ActiveRecord::Migration
  def change
    add_reference :microposts, :reply_to, index: true
  end
end
